---
layout: about
title: About Us
author: Metadiapason
image: https://gitlab.com/metadiapason/resources/-/raw/master/people/cover.jpg
signature: #assets/images/about/signature.png
---

Founded in Rome in 2005 by an impulse of Leonardo Zaccone, <b>MetaDiapason</b> is actually spread in different cities between Italy and Germany.
Originally started as an informal group of musicians with the aim to bring the sound out of the stage, over the years the peculiar formation opened up to people coming from different fields and started new experimentations, branching in an ensemble specialized in sound performances and in a collective focused on interactive installations.

<b>MetaDiapason</b> Ensemble performed at Università di Roma Tor Vergata (2003), at Emufest for Electronic Music Festival of Santa Cecilia Conservatory in Rome (2006 - 2007) for MusikHausSpiel in Berlin (2008), Italian Pavilion Expo in Shanghai (2010), at Nuova Consonanza Festival in Rome - broadcasted on Rai Radio3 (2012), at Festival ArteScienza in Rome (2015). 
<b>MetaDiapason Collective</b> exhibited at Munich, Berlin, Rome, Terni, Vilnius, Ancona, and is partner of RGB Light Experience – Roma Glocal Brightness (2016-2019). 
